from django.db import models


class Call(models.Model):
    phone = models.CharField(max_length=20)
    date = models.DateTimeField()
    call = models.FileField(upload_to='call/%Y/%m/%d')

    def __str__(self):
        return(self.phone)
